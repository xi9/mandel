use clap::{App, Arg};
use complex::{Complex, Magnitude};
use png::{HasParameters, Writer};
use std::error::Error;
use std::fs::File;
use std::io::BufWriter;
use std::path::PathBuf;
use rayon::prelude::*;

const COLORSCHEME_DEFAULT: Colorscheme = Colorscheme::Grey;
const DEPTH_DEFAULT: usize = 100;
const OUTPUT_DEFAULT: &str = "./mandel.png";
const SIZE_DEFAULT: ScreenSize = ScreenSize::new(600, 400);
const VIEW_DEFAULT: ComplexPlaneView = ComplexPlaneView {
    data: [Complex::new(-2f64, -1f64), Complex::new(1f64, 1f64)],
};

#[derive(Debug, Clone)]
struct AppContext {
    colorscheme: Colorscheme,
    view: ComplexPlaneView,
    size: ScreenSize,
    output: PathBuf,
    depth: usize,
    verbosity: usize,
}

#[derive(Debug, Clone)]
enum Colorscheme {
    Grey,
    GreyFilled,
    Eclipse,
    Cool,
    Hot,
    Rainbow,
}

impl Colorscheme {
    // takes a normalized floating-point value (i. e. [0, 1] in set notation on the real numbers)
    // and produces a PixelRGBA with color mapped to it.
    fn normalized_to_pixel(&self, i: Option<f64>) -> PixelRGBA {
        match self {
            Self::Grey => {
                if let Some(f) = i {
                    // were mapping to values with a maximum of 255, we want to save the most
                    // intense color (black) for the members of the mandelbrot set (inputs of the
                    // None Option variant)
                    let fill = (254.0 * (1.0 - f)) as u8;
                    PixelRGBA(fill, fill, fill, 255)
                } else {
                    PixelRGBA(255, 255, 255, 255)
                }
            }
            Self::GreyFilled => {
                if let Some(f) = i {
                    let fill = (254.0 * (1.0 - f)) as u8;
                    PixelRGBA(fill, fill, fill, 255)
                } else {
                    PixelRGBA(0, 0, 0, 255)
                }
            }
            Self::Eclipse => {
                if let Some(f) = i {
                    let fill = (255.0 * f) as u8;
                    PixelRGBA(fill, fill, fill, 255)
                } else {
                    PixelRGBA(0, 0, 0, 255)
                }
            }
            Self::Cool => todo!(),
            Self::Hot => todo!(),
            Self::Rainbow => todo!(),
        }
    }
}

#[derive(Debug, Clone)]
struct ComplexField {
    data: Vec<Vec<Complex<f64>>>,
}

impl ComplexField {
    fn process(self, depth: usize) -> MandelbrotResults {
        let results: Vec<Option<usize>> = self
            .data
            .par_iter()
            .map(|row| {
                row.into_iter()
                    .map(|&c| process_mandelbrot(c, depth))
                    .collect()
            })
            .collect::<Vec<Vec<Option<usize>>>>()
            .into_iter()
            .flatten()
            .collect();
        MandelbrotResults::Usize(results)
    }
}

impl ComplexField {
    fn generate(s: ScreenSize, c: ComplexPlaneView) -> Self {
        let mut out = Vec::with_capacity(s.rows());
        let mut current = Complex::new(c.upper_left().real, c.lower_right().imaginary);
        let d_r = c.width() / s.cols() as f64;
        let d_i = Complex::new(0f64, c.height() / s.rows() as f64);
        for _row in 0..s.rows() {
            let mut temp = Vec::with_capacity(s.cols());
            for _col in 0..s.cols() {
                temp.push(current);
                current += d_r;
            }
            out.push(temp);
            current.real = c.upper_left().real;
            current += d_i;
        }
        Self { data: out }
    }
}

#[derive(Debug, Copy, Clone)]
struct ComplexPlaneView {
    data: [Complex<f64>; 2],
}

impl ComplexPlaneView {
    fn new(corner1: Complex<f64>, corner2: Complex<f64>) -> Self {
        let upper_left = Complex::new(
            f64::min(corner1.real, corner2.real),
            f64::max(corner1.imaginary, corner2.imaginary),
        );
        let lower_right = Complex::new(
            f64::max(corner1.real, corner2.real),
            f64::min(corner1.imaginary, corner2.imaginary),
        );
        Self {
            data: [upper_left, lower_right],
        }
    }

    fn upper_left(&self) -> Complex<f64> {
        self.data[0]
    }

    fn lower_right(&self) -> Complex<f64> {
        self.data[1]
    }

    fn width(&self) -> f64 {
        self.lower_right().real - self.upper_left().real
    }

    fn height(&self) -> f64 {
        self.upper_left().imaginary - self.lower_right().imaginary
    }
}

enum MandelbrotResults {
    F64(Vec<Option<f64>>),
    Usize(Vec<Option<usize>>),
}

impl MandelbrotResults {
    fn as_floats(self) -> Vec<Option<f64>> {
        match self {
            Self::F64(v) => v,
            Self::Usize(v) => {
                let values: Vec<usize> = v
                    .iter()
                    .filter(|r| r.is_some())
                    .map(|r| r.unwrap())
                    .collect();
                let max: usize = values
                    .iter()
                    .max()
                    .expect("Set did not have a maximum")
                    .clone();
                let min: usize = values
                    .iter()
                    .min()
                    .expect("Set did not have a minimum")
                    .clone();
                let normalize = |u: usize| -> f64 { (u - min) as f64 / (max - min) as f64 };
                v.into_iter()
                    .map(|o| {
                        if let Some(u) = o {
                            Some(normalize(u))
                        } else {
                            None
                        }
                    })
                    .collect()
            }
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct PixelRGBA(u8, u8, u8, u8);

impl PixelRGBA {
    fn to_bytes(self) -> [u8; 4] {
        [self.0, self.1, self.2, self.3]
    }
}

impl From<PixelRGBA> for Vec<u8> {
    fn from(i: PixelRGBA) -> Self {
        vec![i.0, i.1, i.2, i.3]
    }
}

#[derive(Debug, Copy, Clone)]
struct ScreenSize {
    data: [usize; 2],
}

impl ScreenSize {
    const fn new(width: usize, height: usize) -> Self {
        ScreenSize {
            data: [width, height],
        }
    }

    fn cols(&self) -> usize {
        self.data[0]
    }

    fn rows(&self) -> usize {
        self.data[1]
    }
}

/// processes the command-line input using `clap` and returns an `AppContext`.
fn get_context() -> Result<AppContext, Box<dyn Error>> {
    let matches = App::new("Mandel")
        .version("1.0")
        .author("Matthew Wilson, matt@highmesa.io")
        .about("Program to plot the Mandelbrot set")
        .arg(
            Arg::with_name("colorscheme")
                .short("c")
                .takes_value(true)
                .possible_value("grey")
                .possible_value("grey-filled")
                .possible_value("eclipse")
                .possible_value("cool")
                .possible_value("hot")
                .possible_value("rainbow")
                .help("colorscheme to apply"),
        )
        .arg(
            Arg::with_name("depth")
                .short("d")
                .takes_value(true)
                .help("max number of iterations"),
        )
        .arg(
            Arg::with_name("output")
                .short("o")
                .takes_value(true)
                .help("output file path"),
        )
        .arg(
            Arg::with_name("size")
                .short("s")
                .takes_value(true)
                .number_of_values(2)
                .help("sets the output image size"),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .multiple(true)
                .help("sets the level of verbosity"),
        )
        .arg(
            Arg::with_name("view")
                .short("V")
                .takes_value(true)
                .number_of_values(2)
                .allow_hyphen_values(true)
                .help("sets the complex viewplane"),
        )
        .get_matches();

    let colorscheme = if let Some(s) = matches.value_of("colorscheme") {
        match s {
            "grey" => Colorscheme::Grey,
            "grey-filled" => Colorscheme::GreyFilled,
            "eclipse" => Colorscheme::Eclipse,
            "cool" => Colorscheme::Cool,
            "hot" => Colorscheme::Hot,
            "rainbow" => Colorscheme::Rainbow,
            _ => COLORSCHEME_DEFAULT,
        }
    } else {
        COLORSCHEME_DEFAULT
    };

    let depth = if let Some(s) = matches.value_of("depth") {
        str::parse::<usize>(s)?
    } else {
        DEPTH_DEFAULT
    };

    let output = if let Some(s) = matches.value_of("output") {
        PathBuf::from(s)
    } else {
        PathBuf::from(OUTPUT_DEFAULT)
    };

    let size = if let Some(v) = matches.values_of("size") {
        let data = v
            .into_iter()
            .enumerate()
            .map(|(c, s)| {
                if let Ok(f) = str::parse::<usize>(s) {
                    f
                } else {
                    SIZE_DEFAULT.data[c]
                }
            })
            .collect::<Vec<usize>>();
        ScreenSize::new(data[0], data[1])
    } else {
        SIZE_DEFAULT
    };

    let verbosity = matches.occurrences_of("verbose") as usize;

    let view = if let Some(v) = matches.values_of("view") {
        let data = v
            .into_iter()
            .enumerate()
            .map(|(c, s)| {
                if let Ok(f) = str::parse::<Complex<f64>>(s) {
                    f
                } else {
                    VIEW_DEFAULT.data[c]
                }
            })
            .collect::<Vec<Complex<f64>>>();
        ComplexPlaneView::new(data[0], data[1])
    } else {
        VIEW_DEFAULT
    };

    Ok(AppContext {
        colorscheme,
        depth,
        output,
        size,
        verbosity,
        view,
    })
}

/// If the number is a part of the Mandelbrot set, returns `None`.
/// If the number isn't a part of the Mandelbrot set, returns the `Some(usize)` number of iterations
/// it took to make that determination.
fn process_mandelbrot(c: Complex<f64>, d: usize) -> Option<usize> {
    let f = |z: Complex<f64>| -> Complex<f64> { z * z + c };
    let mut i: usize = 0;
    let mut z = Complex::new(0.0, 0.0);
    while i < d {
        if z.magnitude() < 2f64 {
            z = f(z);
            i += 1;
        } else {
            return Some(i);
        }
    }
    None
}

fn make_image(p: Vec<PixelRGBA>, c: AppContext) -> Result<(), Box<dyn Error>> {
    let file = File::create(c.output)?;
    let mut w = BufWriter::new(file);
    let mut encoder = png::Encoder::new(w, c.size.cols() as u32, c.size.rows() as u32);
    encoder.set(png::ColorType::RGBA).set(png::BitDepth::Eight);
    let mut writer = encoder.write_header()?;
    writer.write_image_data(
        &(p.into_iter()
            .map(|p| Vec::<u8>::from(p))
            .flatten()
            .collect::<Vec<u8>>()),
    )?;
    Ok(())
}

fn main() -> Result<(), Box<dyn Error>> {
    let context = get_context()?;
    macro_rules! verb {
        (
            $($input:tt)*
        ) => {
            if context.verbosity > 0 {
                println!( $($input)* );
            }
        }
    }
    verb! {
        "context acquired.\n\
        ---\n\
        colorscheme: {:?}\n\
        iteration depth: {}\n\
        output file: {:?}\n\
        screen size: {:?}\n\
        verbosity: {}\n\
        complex view: {:?}\n\
        ---\n",
        context.colorscheme, context.depth, context.output, context.size, context.verbosity, context.view,
    };
    let field: ComplexField = ComplexField::generate(context.size, context.view);
    verb! {
        "done, generated {} elements",
        context.size.rows() * context.size.cols()
    };
    let results: Vec<Option<f64>> = field.process(context.depth).as_floats();
    verb! { "colormapping results..." };
    let pixels: Vec<PixelRGBA> = results
        .par_iter()
        .map(|&r| context.colorscheme.normalized_to_pixel(r))
        .collect();
    make_image(pixels, context)?;
    Ok(())
}
